#ifndef IMAGE_OPERATIONS_H
#define IMAGE_OPERATIONS_H

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <vector>
#include <string>

using namespace cv;
using namespace std;

Mat filter(Mat& image, vector<vector<int>> &mask, bool absolute = true);
Mat addImages(Mat& imageA, Mat& imageB);
Mat subtractImages(Mat& imageA, Mat& imageB);
Mat maxOfImages(Mat& imageA, Mat& imageB);
Mat normalizeImage(Mat& image);
Mat nonMaximumSuppresion(Mat& image, Mat& gradientIndices);
Mat arctanOfImages(Mat& imageA, Mat& imageB);
void hysteresisFollowing(Mat& image, int i, int j, int t1, int t2);
Mat hysteresis(Mat& image, int t1, int t2);
Mat binarize(Mat& image);
Mat colorize(Mat& binary, Mat& image);
vector<vector<Point>> findCurves(Mat& img);
double countNonBlackPercentage(Mat& image);


vector<Mat> loadImages(string postfix = "", int mode = 0, string directory = "");
void saveImages(vector<Mat>& imgs, string postfix, string directory = "");
void saveImages(vector<vector<Mat>>& imgs, vector<string> postfixes, string directory = "");

#endif