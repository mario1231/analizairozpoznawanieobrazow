#include <string>

#include "constants.h"

using namespace std;

ImagePath::ImagePath(string _name, string _ext) : name(_name), ext(_ext) {}

vector<ImagePath> imagePaths({
    ImagePath("A", "png"),
    ImagePath("B", "png"),
    ImagePath("C", "png")
});

vector<vector<int> > gaussianMask = {
    {2, 4, 5, 4, 2},
    {4, 9, 12, 9, 4},
    {5, 12, 15, 12, 5},
    {4, 9, 12, 9, 4},
    {2, 4, 5, 4, 2} 
};

vector<vector<vector<int> > > kirschMasks = {
    {
        { -1, 0, 1},
        { -2, 0, 2},
        { -1, 0, 1} 
    }, {
        { 1, 2, 1},
        { 0, 0, 0},
        { -1, -2, -1}
    }
};