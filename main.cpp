#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include "constants.h"
#include "image-oprations.h"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace cv;
using namespace std;


//poszczegolne etapy zadania:
void extractChannels() {
    vector<Mat> mono = loadImages("");
    vector<Mat> images = loadImages("", 1);
    vector<vector<Mat>> channels;
    
    for (Mat image : images) {
        Mat r, g, b;
        extractChannel(image, r, 2);
        extractChannel(image, g, 1);
        extractChannel(image, b, 0);

        channels.push_back(vector<Mat>({r, g, b}));
    }
    
    saveImages(channels, vector<string>({"_1_R", "_1_G", "_1_B"}), "rgb");

    for (vector<Mat> v : channels) {
        v[0] *= 0.3;
        v[1] *= 0.6;
        v[2] *= 0.1;
    }

    saveImages(channels, vector<string>({"_1_R", "_1_G", "_1_B"}), "rgb-361");

    saveImages(mono, "_1", "mono");
}

void gaussFilter() {
    vector<Mat> r = loadImages("_1_R", 0, "rgb");
    vector<Mat> g = loadImages("_1_G", 0, "rgb");
    vector<Mat> b = loadImages("_1_B", 0, "rgb");
    vector<Mat> r1 = loadImages("_1_R", 0, "rgb-361");
    vector<Mat> g1 = loadImages("_1_G", 0, "rgb-361");
    vector<Mat> b1 = loadImages("_1_B", 0, "rgb-361"); 
    vector<Mat> mono = loadImages("_1", 0, "mono"); 

    for (int i = 0; i < r.size(); ++i) {
        r[i] = filter(r[i], gaussianMask);
        g[i] = filter(g[i], gaussianMask);
        b[i] = filter(b[i], gaussianMask);
        r1[i] = filter(r1[i], gaussianMask);
        g1[i] = filter(g1[i], gaussianMask);
        b1[i] = filter(b1[i], gaussianMask);
        mono[i] = filter(mono[i], gaussianMask);
    }

    saveImages(r, "_2_R_gauss", "rgb");
    saveImages(g, "_2_G_gauss", "rgb");
    saveImages(b, "_2_B_gauss", "rgb");
    saveImages(r1, "_2_R_gauss", "rgb-361");
    saveImages(g1, "_2_G_gauss", "rgb-361");
    saveImages(b1, "_2_B_gauss", "rgb-361");
    saveImages(mono, "_2_gauss", "mono");
}

void kirschFilter() {
    vector<Mat> r = loadImages("_2_R_gauss", 0, "rgb");
    vector<Mat> g = loadImages("_2_G_gauss", 0, "rgb");
    vector<Mat> b = loadImages("_2_B_gauss", 0, "rgb");
    vector<Mat> r1 = loadImages("_2_R_gauss", 0, "rgb-361");
    vector<Mat> g1 = loadImages("_2_G_gauss", 0, "rgb-361");
    vector<Mat> b1 = loadImages("_2_B_gauss", 0, "rgb-361");
    vector<Mat> mono = loadImages("_2_gauss", 0, "mono");

    vector<Mat> r2 = loadImages("_2_R_gauss", 0, "rgb");
    vector<Mat> g2 = loadImages("_2_G_gauss", 0, "rgb");
    vector<Mat> b2 = loadImages("_2_B_gauss", 0, "rgb");
    vector<Mat> r12 = loadImages("_2_R_gauss", 0, "rgb-361");
    vector<Mat> g12 = loadImages("_2_G_gauss", 0, "rgb-361");
    vector<Mat> b12 = loadImages("_2_B_gauss", 0, "rgb-361");
    vector<Mat> mono2 = loadImages("_2_gauss", 0, "mono");

    for (int i = 0; i < r.size(); ++i) {
        r[i] = filter(r[i], kirschMasks[0]);
        g[i] = filter(g[i], kirschMasks[0]);
        b[i] = filter(b[i], kirschMasks[0]);
        r1[i] = filter(r[i], kirschMasks[0]);
        g1[i] = filter(g[i], kirschMasks[0]);
        b1[i] = filter(b[i], kirschMasks[0]);
        mono[i] = filter(mono[i], kirschMasks[0]);

        r2[i] = filter(r2[i], kirschMasks[1]);
        g2[i] = filter(g2[i], kirschMasks[1]);
        b2[i] = filter(b2[i], kirschMasks[1]);
        r12[i] = filter(r2[i], kirschMasks[1]);
        g12[i] = filter(g2[i], kirschMasks[1]);
        b12[i] = filter(b2[i], kirschMasks[1]);
        mono2[i] = filter(mono2[i], kirschMasks[1]);
    }

    saveImages(r, "_3_1_R_kirsch", "rgb");
    saveImages(g, "_3_1_G_kirsch", "rgb");
    saveImages(b, "_3_1_B_kirsch", "rgb");
    saveImages(r1, "_3_1_R_kirsch", "rgb-361");
    saveImages(g1, "_3_1_G_kirsch", "rgb-361");
    saveImages(b1, "_3_1_B_kirsch", "rgb-361");
    saveImages(mono, "_3_1_kirsch", "mono");

    saveImages(r2, "_3_2_R_kirsch", "rgb");
    saveImages(g2, "_3_2_G_kirsch", "rgb");
    saveImages(b2, "_3_2_B_kirsch", "rgb");
    saveImages(r12, "_3_2_R_kirsch", "rgb-361");
    saveImages(g12, "_3_2_G_kirsch", "rgb-361");
    saveImages(b12, "_3_2_B_kirsch", "rgb-361");
    saveImages(mono2, "_3_2_kirsch", "mono");
}

void normalizeKirsch() {
    vector<Mat> r = loadImages("_3_1_R_kirsch", 0, "rgb");
    vector<Mat> g = loadImages("_3_1_G_kirsch", 0, "rgb");
    vector<Mat> b = loadImages("_3_1_B_kirsch", 0, "rgb");
    vector<Mat> r1 = loadImages("_3_1_R_kirsch", 0, "rgb-361");
    vector<Mat> g1 = loadImages("_3_1_G_kirsch", 0, "rgb-361");
    vector<Mat> b1 = loadImages("_3_1_B_kirsch", 0, "rgb-361");
    vector<Mat> mono = loadImages("_3_1_kirsch", 0, "mono");

    vector<Mat> r2 = loadImages("_3_2_R_kirsch", 0, "rgb");
    vector<Mat> g2 = loadImages("_3_2_G_kirsch", 0, "rgb");
    vector<Mat> b2 = loadImages("_3_2_B_kirsch", 0, "rgb");
    vector<Mat> r12 = loadImages("_3_2_R_kirsch", 0, "rgb-361");
    vector<Mat> g12 = loadImages("_3_2_G_kirsch", 0, "rgb-361");
    vector<Mat> b12 = loadImages("_3_2_B_kirsch", 0, "rgb-361");
    vector<Mat> mono2 = loadImages("_3_2_kirsch", 0, "mono");

    for (int i = 0; i < r.size(); ++i) {
        r[i] = normalizeImage(r[i]);
        g[i] = normalizeImage(g[i]);
        b[i] = normalizeImage(b[i]);
        r1[i] = normalizeImage(r1[i]);
        g1[i] = normalizeImage(g1[i]);
        b1[i] = normalizeImage(b1[i]);
        mono[i] = normalizeImage(mono[i]);

        r2[i] = normalizeImage(r2[i]);
        g2[i] = normalizeImage(g2[i]);
        b2[i] = normalizeImage(b2[i]);
        r12[i] = normalizeImage(r12[i]);
        g12[i] = normalizeImage(g12[i]);
        b12[i] = normalizeImage(b12[i]);
        mono2[i] = normalizeImage(mono2[i]);
    }

    saveImages(r, "_4_1_R_kirsch_normalized", "rgb");
    saveImages(g, "_4_1_G_kirsch_normalized", "rgb");
    saveImages(b, "_4_1_B_kirsch_normalized", "rgb");
    saveImages(r1, "_4_1_R_kirsch_normalized", "rgb-361");
    saveImages(g1, "_4_1_G_kirsch_normalized", "rgb-361");
    saveImages(b1, "_4_1_B_kirsch_normalized", "rgb-361");
    saveImages(mono, "_4_1_kirsch_normalized", "mono");

    saveImages(r2, "_4_2_R_kirsch_normalized", "rgb");
    saveImages(g2, "_4_2_G_kirsch_normalized", "rgb");
    saveImages(b2, "_4_2_B_kirsch_normalized", "rgb");
    saveImages(r12, "_4_2_R_kirsch_normalized", "rgb-361");
    saveImages(g12, "_4_2_G_kirsch_normalized", "rgb-361");
    saveImages(b12, "_4_2_B_kirsch_normalized", "rgb-361");
    saveImages(mono2, "_4_2_kirsch_normalized", "mono");
}

void maxOfKirsch() {
    vector<Mat> r = loadImages("_4_1_R_kirsch_normalized", 0, "rgb");
    vector<Mat> g = loadImages("_4_1_G_kirsch_normalized", 0, "rgb");
    vector<Mat> b = loadImages("_4_1_B_kirsch_normalized", 0, "rgb");
    vector<Mat> r1 = loadImages("_4_1_R_kirsch_normalized", 0, "rgb-361");
    vector<Mat> g1 = loadImages("_4_1_G_kirsch_normalized", 0, "rgb-361");
    vector<Mat> b1 = loadImages("_4_1_B_kirsch_normalized", 0, "rgb-361");
    vector<Mat> mono = loadImages("_4_1_kirsch_normalized", 0, "mono");

    vector<Mat> r2 = loadImages("_4_2_R_kirsch_normalized", 0, "rgb");
    vector<Mat> g2 = loadImages("_4_2_G_kirsch_normalized", 0, "rgb");
    vector<Mat> b2 = loadImages("_4_2_B_kirsch_normalized", 0, "rgb");
    vector<Mat> r12 = loadImages("_4_2_R_kirsch_normalized", 0, "rgb-361");
    vector<Mat> g12 = loadImages("_4_2_G_kirsch_normalized", 0, "rgb-361");
    vector<Mat> b12 = loadImages("_4_2_B_kirsch_normalized", 0, "rgb-361");
    vector<Mat> mono2 = loadImages("_4_2_kirsch_normalized", 0, "mono");

    for (int i = 0; i < r.size(); ++i) {
        r[i] = addImages(r[i], r2[i]);
        g[i] = addImages(g[i], g2[i]);
        b[i] = addImages(b[i], b2[i]);
        r1[i] = addImages(r1[i], r12[i]);
        g1[i] = addImages(g1[i], g12[i]);
        b1[i] = addImages(b1[i], b12[i]);
        mono[i] = addImages(mono[i], mono2[i]);
    }

    saveImages(r, "_5_1_R_kirsch_max", "rgb");
    saveImages(g, "_5_1_G_kirsch_max", "rgb");
    saveImages(b, "_5_1_B_kirsch_max", "rgb");
    saveImages(r, "_5_1_R_kirsch_max", "rgb-361");
    saveImages(g, "_5_1_G_kirsch_max", "rgb-361");
    saveImages(b, "_5_1_B_kirsch_max", "rgb-361");
    saveImages(mono, "_5_1_kirsch_max", "mono");
}

void gradientDirections() {
    vector<Mat> r = loadImages("_2_R_gauss", 0, "rgb");
    vector<Mat> g = loadImages("_2_G_gauss", 0, "rgb");
    vector<Mat> b = loadImages("_2_B_gauss", 0, "rgb");
    vector<Mat> r1 = loadImages("_2_R_gauss", 0, "rgb-361");
    vector<Mat> g1 = loadImages("_2_G_gauss", 0, "rgb-361");
    vector<Mat> b1 = loadImages("_2_B_gauss", 0, "rgb-361");
    vector<Mat> mono = loadImages("_2_gauss", 0, "mono");

    vector<Mat> r2 = loadImages("_2_R_gauss", 0, "rgb");
    vector<Mat> g2 = loadImages("_2_G_gauss", 0, "rgb");
    vector<Mat> b2 = loadImages("_2_B_gauss", 0, "rgb");
    vector<Mat> r12 = loadImages("_2_R_gauss", 0, "rgb-361");
    vector<Mat> g12 = loadImages("_2_G_gauss", 0, "rgb-361");
    vector<Mat> b12 = loadImages("_2_B_gauss", 0, "rgb-361");
    vector<Mat> mono2 = loadImages("_2_gauss", 0, "mono");

    vector<Mat> r3, g3, b3, r13, g13, b13, mono3;

    for (int i = 0; i < r.size(); ++i) {
        r[i] = filter(r[i], kirschMasks[0], false);
        g[i] = filter(g[i], kirschMasks[0], false);
        b[i] = filter(b[i], kirschMasks[0], false);
        r1[i] = filter(r1[i], kirschMasks[0], false);
        g1[i] = filter(g1[i], kirschMasks[0], false);
        b1[i] = filter(b1[i], kirschMasks[0], false);
        mono[i] = filter(mono[i], kirschMasks[0], false);

        r2[i] = filter(r2[i], kirschMasks[1], false);
        g2[i] = filter(g2[i], kirschMasks[1], false);
        b2[i] = filter(b2[i], kirschMasks[1], false);
        r12[i] = filter(r12[i], kirschMasks[1], false);
        g12[i] = filter(g12[i], kirschMasks[1], false);
        b12[i] = filter(b12[i], kirschMasks[1], false);
        mono2[i] = filter(mono2[i], kirschMasks[1], false);

        r3.push_back(arctanOfImages(r[i], r2[i]));
        g3.push_back(arctanOfImages(g[i], g2[i]));
        b3.push_back(arctanOfImages(b[i], b2[i]));
        r13.push_back(arctanOfImages(r1[i], r12[i]));
        g13.push_back(arctanOfImages(g1[i], g12[i]));
        b13.push_back(arctanOfImages(b1[i], b12[i]));
        mono3.push_back(arctanOfImages(mono[i], mono2[i]));
    }

    saveImages(r3, "_6_r_gradient_directions", "rgb");
    saveImages(g3, "_6_g_gradient_directions", "rgb");
    saveImages(b3, "_6_b_gradient_directions", "rgb");
    saveImages(r13, "_6_r_gradient_directions", "rgb-361");
    saveImages(g13, "_6_g_gradient_directions", "rgb-361");
    saveImages(b13, "_6_b_gradient_directions", "rgb-361");
    saveImages(mono3, "_6_gradient_directions", "mono");
}

void nonMaximum() {
    vector<Mat> r = loadImages("_5_1_R_kirsch_max", 0, "rgb");
    vector<Mat> g = loadImages("_5_1_G_kirsch_max", 0, "rgb");
    vector<Mat> b = loadImages("_5_1_B_kirsch_max", 0, "rgb");
    vector<Mat> r1 = loadImages("_5_1_R_kirsch_max", 0, "rgb-361");
    vector<Mat> g1 = loadImages("_5_1_G_kirsch_max", 0, "rgb-361");
    vector<Mat> b1 = loadImages("_5_1_B_kirsch_max", 0, "rgb-361");
    vector<Mat> mono = loadImages("_5_1_kirsch_max", 0, "mono");

    vector<Mat> r2 = loadImages("_6_R_gradient_directions", 0, "rgb");
    vector<Mat> g2 = loadImages("_6_G_gradient_directions", 0, "rgb");
    vector<Mat> b2 = loadImages("_6_B_gradient_directions", 0, "rgb");
    vector<Mat> r12 = loadImages("_6_R_gradient_directions", 0, "rgb-361");
    vector<Mat> g12 = loadImages("_6_G_gradient_directions", 0, "rgb-361");
    vector<Mat> b12 = loadImages("_6_B_gradient_directions", 0, "rgb-361");
    vector<Mat> mono2 = loadImages("_6_gradient_directions", 0, "mono");

    for (int i = 0; i < r.size(); ++i) {
        r[i] = nonMaximumSuppresion(r[i], r2[i]);
        g[i] = nonMaximumSuppresion(g[i], g2[i]);
        b[i] = nonMaximumSuppresion(b[i], b2[i]);
        r1[i] = nonMaximumSuppresion(r1[i], r12[i]);
        g1[i] = nonMaximumSuppresion(g1[i], g12[i]);
        b1[i] = nonMaximumSuppresion(b1[i], b12[i]);
        mono[i] = nonMaximumSuppresion(mono[i], mono2[i]);
    }

    saveImages(r, "_7_R_non_maximum", "rgb");
    saveImages(g, "_7_G_non_maximum", "rgb");
    saveImages(b, "_7_B_non_maximum", "rgb");
    saveImages(r1, "_7_R_non_maximum", "rgb-361");
    saveImages(g1, "_7_G_non_maximum", "rgb-361");
    saveImages(b1, "_7_B_non_maximum", "rgb-361");
    saveImages(mono, "_7_non_maximum", "mono");
}

void histerize(vector<vector<vector<int>>> thresholds) {
    vector<Mat> r = loadImages("_7_R_non_maximum", 0, "rgb");
    vector<Mat> g = loadImages("_7_G_non_maximum", 0, "rgb");
    vector<Mat> b = loadImages("_7_B_non_maximum", 0, "rgb");
    vector<Mat> r1 = loadImages("_7_R_non_maximum", 0, "rgb-361");
    vector<Mat> g1 = loadImages("_7_G_non_maximum", 0, "rgb-361");
    vector<Mat> b1 = loadImages("_7_B_non_maximum", 0, "rgb-361");
    vector<Mat> mono = loadImages("_7_non_maximum", 0, "mono");

    for (int i = 0; i < r.size(); ++i) {
        r[i] = hysteresis(r[i], thresholds[i][0][0], thresholds[i][0][1]);
        g[i] = hysteresis(g[i], thresholds[i][1][0], thresholds[i][1][1]);
        b[i] = hysteresis(b[i], thresholds[i][2][0], thresholds[i][2][1]);
        r1[i] = hysteresis(r1[i], thresholds[i][3][0], thresholds[i][3][1]);
        g1[i] = hysteresis(g1[i], thresholds[i][4][0], thresholds[i][4][1]);
        b1[i] = hysteresis(b1[i], thresholds[i][5][0], thresholds[i][5][1]);
        mono[i] = hysteresis(mono[i], thresholds[i][6][0], thresholds[i][6][1]);
    }

    saveImages(r, "_8_R_hysteresis", "rgb");
    saveImages(g, "_8_G_hysteresis", "rgb");
    saveImages(b, "_8_B_hysteresis", "rgb");
    saveImages(r1, "_8_R_hysteresis", "rgb-361");
    saveImages(g1, "_8_G_hysteresis", "rgb-361");
    saveImages(b1, "_8_B_hysteresis", "rgb-361");
    saveImages(mono, "_9_hysteresis", "mono");
}

void histerizeSum() {
    vector<Mat> r = loadImages("_8_R_hysteresis", 0, "rgb");
    vector<Mat> g = loadImages("_8_G_hysteresis", 0, "rgb");
    vector<Mat> b = loadImages("_8_B_hysteresis", 0, "rgb");
    vector<Mat> r2 = loadImages("_8_R_hysteresis", 0, "rgb-361");
    vector<Mat> g2 = loadImages("_8_G_hysteresis", 0, "rgb-361");
    vector<Mat> b2 = loadImages("_8_B_hysteresis", 0, "rgb-361");

    for (int i = 0; i < r.size(); ++i) {
        r[i] = maxOfImages(r[i], g[i]);
        r[i] = maxOfImages(r[i], b[i]);
        r2[i] = maxOfImages(r2[i], g2[i]);
        r2[i] = maxOfImages(r2[i], b2[i]);
    }

    saveImages(r, "_9_sum_hysteresis", "rgb");
    saveImages(r2, "_9_sum_hysteresis", "rgb-361");
}

void separateCurves() {
    vector<Mat> img = loadImages("_9_sum_hysteresis", 0, "rgb");
    vector<Mat> img2 = loadImages("_9_sum_hysteresis", 0, "rgb-361");
    vector<Mat> mono = loadImages("_9_hysteresis", 0, "mono");
    RNG rng(rand());

    for (int i = 0; i < img.size(); ++i) {
        vector<vector<Point>> curves = findCurves(img[i]);

        //Rysowanie znalezionych krzywych na nowym obrazie
        Mat drawing = Mat::zeros(img[i].size(), CV_8UC3);

        for(int j = 0; j < curves.size(); j++) {
            Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
            drawContours(drawing, curves, j, color, 1 );
        }

        int sum = 0;
        for (vector<Point> p : curves) 
            sum += p.size();

        cout << "Image (rgb) of number " << i+1 << ", curves: " << sum << endl;

        img[i] = drawing;
    }
    saveImages(img, "_10_curves", "rgb");

    for (int i = 0; i < img2.size(); ++i) {
        vector<vector<Point>> curves = findCurves(img2[i]);

        //Rysowanie znalezionych krzywych na nowym obrazie
        Mat drawing = Mat::zeros(img2[i].size(), CV_8UC3);

        for(int j = 0; j < curves.size(); j++) {
            Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
            drawContours(drawing, curves, j, color, 1 );
        }

        int sum = 0;
        for (vector<Point> p : curves) 
            sum += p.size();

        cout << "Image (rgb-361) of number " << i+1 << ", curves: " << sum << endl;

        img2[i] = drawing;
    }
    saveImages(img2, "_10_curves", "rgb-361");

    for (int i = 0; i < mono.size(); ++i) {
        vector<vector<Point>> curves = findCurves(mono[i]);

        //Rysowanie znalezionych krzywych na nowym obrazie
        Mat drawing = Mat::zeros(mono[i].size(), CV_8UC3);

        for(int j = 0; j < curves.size(); j++) {
            Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
            drawContours(drawing, curves, j, color, 1 );
        }

        int sum = 0;
        for (vector<Point> p : curves) 
            sum += p.size();

        cout << "Image (mono) of number " << i+1 << ", curves: " << sum << endl;

        mono[i] = drawing;
    }

    saveImages(mono, "_10_curves", "mono");
}

void rdp(int epsilon) {
    vector<Mat> img = loadImages("_9_sum_hysteresis", 0, "rgb");
    vector<Mat> img2 = loadImages("_9_sum_hysteresis", 0, "rgb-361");
    vector<Mat> mono = loadImages("_9_hysteresis", 0, "mono");
    RNG rng(rand());

    for (int i = 0; i < img.size(); ++i) {
        vector<vector<Point>> curves = findCurves(img[i]);
        vector<vector<Point>> reducedCures(curves.size());

        //Redukcja krzywych
        for(size_t k = 0; k < curves.size(); k++)
            approxPolyDP(Mat(curves[k]), reducedCures[k], epsilon, false);

        Mat drawing = Mat::zeros(img[i].size(), CV_8UC3);

        for(int j = 0; j < curves.size(); j++) {
            Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
            drawContours(drawing, reducedCures, j, color, 1);
        }

        int sum = 0;
        for (vector<Point> p : reducedCures) 
            sum += p.size();

        cout << "Image (rgb) of number " << i+1 << ", reduced curves: " << sum << endl;

        img[i] = drawing;
    }
    saveImages(img, string("_10_RDP_" + to_string(epsilon)), "rgb");

    for (int i = 0; i < img2.size(); ++i) {
        vector<vector<Point>> curves = findCurves(img2[i]);
        vector<vector<Point>> reducedCures(curves.size());

        //Redukcja krzywych
        for(size_t k = 0; k < curves.size(); k++)
            approxPolyDP(Mat(curves[k]), reducedCures[k], epsilon, false);

        Mat drawing = Mat::zeros(img[i].size(), CV_8UC3);

        for(int j = 0; j < curves.size(); j++) {
            Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
            drawContours(drawing, reducedCures, j, color, 1);
        }

        int sum = 0;
        for (vector<Point> p : reducedCures) 
            sum += p.size();

        cout << "Image (rgb-361) of number " << i+1 << ", reduced curves: " << sum << endl;

        img2[i] = drawing;
    }
    saveImages(img2, string("_10_RDP_" + to_string(epsilon)), "rgb-361");

    for (int i = 0; i < mono.size(); ++i) {
        vector<vector<Point>> curves = findCurves(mono[i]);
        vector<vector<Point>> reducedCures(curves.size());

        //Redukcja krzywych
        for(size_t k = 0; k < curves.size(); k++)
            approxPolyDP(Mat(curves[k]), reducedCures[k], epsilon, false);

        Mat drawing = Mat::zeros(mono[i].size(), CV_8UC3);

        for(int j = 0; j < curves.size(); j++) {
            Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
            drawContours(drawing, reducedCures, j, color, 1);
        }

        int sum = 0;
        for (vector<Point> p : reducedCures) 
            sum += p.size();

        cout << "Image (mono) of number " << i+1 << ", reduced curves: " << sum << endl;

        mono[i] = drawing;
    }
    saveImages(mono, string("_10_RDP_" + to_string(epsilon)), "mono");
}

void colorize() {
    vector<Mat> img = loadImages("_9_sum_hysteresis", 1, "rgb");
    vector<Mat> img361 = loadImages("_9_sum_hysteresis", 1, "rgb-361");
    vector<Mat> mono = loadImages("_9_hysteresis", 1, "mono");
    vector<Mat> img_1 = loadImages("_10_RDP_1", 1, "rgb");
    vector<Mat> img361_1 = loadImages("_10_RDP_1", 1, "rgb-361");
    vector<Mat> mono_1 = loadImages("_10_RDP_1", 1, "mono");
    vector<Mat> img_5 = loadImages("_10_RDP_5", 1, "rgb");
    vector<Mat> img361_5 = loadImages("_10_RDP_5", 1, "rgb-361");
    vector<Mat> mono_5 = loadImages("_10_RDP_5", 1, "mono");
    vector<Mat> img_10 = loadImages("_10_RDP_10", 1, "rgb");
    vector<Mat> img361_10 = loadImages("_10_RDP_10", 1, "rgb-361");
    vector<Mat> mono_10 = loadImages("_10_RDP_10", 1, "mono");

    vector<Mat> img2 = loadImages("", 1);
    vector<Mat> img3612 = loadImages("", 1);
    vector<Mat> mono2 = loadImages("", 1);
    vector<Mat> img_12 = loadImages("", 1);
    vector<Mat> img361_12 = loadImages("", 1);
    vector<Mat> mono_12 = loadImages("", 1);
    vector<Mat> img_52 = loadImages("", 1);
    vector<Mat> img361_52 = loadImages("", 1);
    vector<Mat> mono_52 = loadImages("", 1);
    vector<Mat> img_102 = loadImages("", 1);
    vector<Mat> img361_102 = loadImages("", 1);
    vector<Mat> mono_102 = loadImages("", 1);

    for (int i = 0; i < img.size(); ++i) {
        Mat binarized = binarize(img[i]);
        Mat binarized361 = binarize(img361[i]);
        Mat binarizedMono = binarize(mono[i]);
        Mat binarized_1 = binarize(img_1[i]);
        Mat binarized361_1 = binarize(img361_1[i]);
        Mat binarizedMono_1 = binarize(mono_1[i]);
        Mat binarized_5 = binarize(img_5[i]);
        Mat binarized361_5 = binarize(img361_5[i]);
        Mat binarizedMono_5 = binarize(mono_5[i]);
        Mat binarized_10 = binarize(img_10[i]);
        Mat binarized361_10 = binarize(img361_10[i]);
        Mat binarizedMono_10 = binarize(mono_10[i]);

        img2[i] = colorize(binarized, img2[i]);
        img3612[i] = colorize(binarized361, img3612[i]);
        mono2[i] = colorize(binarizedMono, mono2[i]);
        img_12[i] = colorize(binarized_1, img_12[i]);
        img361_12[i] = colorize(binarized361_1, img361_12[i]);
        mono_12[i] = colorize(binarizedMono_1, mono_12[i]);
        img_52[i] = colorize(binarized_5, img_52[i]);
        img361_52[i] = colorize(binarized361_5, img361_52[i]);
        mono_52[i] = colorize(binarizedMono_5, mono_52[i]);
        img_102[i] = colorize(binarized_10, img_102[i]);
        img361_102[i] = colorize(binarized361_10, img361_102[i]);
        mono_102[i] = colorize(binarizedMono_10, mono_102[i]);
    }

    saveImages(img2, "_11_colorized", "rgb");
    saveImages(img3612, "_11_colorized", "rgb-361");
    saveImages(mono2, "_11_colorized", "mono");
    saveImages(img_12, "_11_colorized_1", "rgb");
    saveImages(img361_12, "_11_colorized_1", "rgb-361");
    saveImages(mono_12, "_11_colorized_1", "mono");
    saveImages(img_52, "_11_colorized_5", "rgb");
    saveImages(img361_52, "_11_colorized_5", "rgb-361");
    saveImages(mono_52, "_11_colorized_5", "mono");
    saveImages(img_102, "_11_colorized_10", "rgb");
    saveImages(img361_102, "_11_colorized_10", "rgb-361");
    saveImages(mono_102, "_11_colorized_10", "mono");
}

void difference() {
    //RGB - RGB 361
    vector<Mat> img = loadImages("_9_sum_hysteresis", 1, "rgb");
    vector<Mat> img361 = loadImages("_9_sum_hysteresis", 1, "rgb-361");
    vector<Mat> img_1 = loadImages("_10_RDP_1", 1, "rgb");
    vector<Mat> img361_1 = loadImages("_10_RDP_1", 1, "rgb-361");
    vector<Mat> img_5 = loadImages("_10_RDP_5", 1, "rgb");
    vector<Mat> img361_5 = loadImages("_10_RDP_5", 1, "rgb-361");
    vector<Mat> img_10 = loadImages("_10_RDP_10", 1, "rgb");
    vector<Mat> img361_10 = loadImages("_10_RDP_1", 1, "rgb-361");

    for (int i = 0; i < img.size(); ++i) {
        img[i] = binarize(img[i]);
        img_1[i] = binarize(img_1[i]);
        img_5[i] = binarize(img_5[i]);
        img_10[i] = binarize(img_10[i]);
        img361[i] = binarize(img361[i]);
        img361_1[i] = binarize(img361_1[i]);
        img361_5[i] = binarize(img361_5[i]);
        img361_10[i] = binarize(img361_10[i]);

        double imgBefore = countNonBlackPercentage(img[i]);
        double imgBefore_1 = countNonBlackPercentage(img_1[i]);
        double imgBefore_5 = countNonBlackPercentage(img_5[i]);
        double imgBefore_10 = countNonBlackPercentage(img_10[i]);

        cout << "\n\nRGB - RGB-361:\n";
        img[i] = subtractImages(img[i], img361[i]);
        cout << countNonBlackPercentage(img[i]) / imgBefore * 100 << "%\n";
        img_1[i] = subtractImages(img_1[i], img361_1[i]);
        cout << countNonBlackPercentage(img_1[i]) / imgBefore_1 * 100 << "%\n";
        img_5[i] = subtractImages(img_5[i], img361_5[i]);
        cout << countNonBlackPercentage(img_5[i]) / imgBefore_5 * 100 << "%\n";
        img_10[i] = subtractImages(img_10[i], img361_10[i]);
        cout << countNonBlackPercentage(img_10[i]) / imgBefore_10 * 100 << "%\n";
    }

    saveImages(img, "_12_rgb_minus_rgb361", "difference");
    saveImages(img_1, "_12_rgb_minus_rgb361_1", "difference");
    saveImages(img_5, "_12_rgb_minus_rgb361_5", "difference");
    saveImages(img_10, "_12_rgb_minus_rgb361_10", "difference");

    //RGB - MONO
    img = loadImages("_9_sum_hysteresis", 1, "rgb");
    img361 = loadImages("_9_hysteresis", 1, "mono");
    img_1 = loadImages("_10_RDP_1", 1, "rgb");
    img361_1 = loadImages("_10_RDP_1", 1, "mono");
    img_5 = loadImages("_10_RDP_5", 1, "rgb");
    img361_5 = loadImages("_10_RDP_5", 1, "mono");
    img_10 = loadImages("_10_RDP_10", 1, "rgb");
    img361_10 = loadImages("_10_RDP_10", 1, "mono");

    for (int i = 0; i < img.size(); ++i) {
        img[i] = binarize(img[i]);
        img_1[i] = binarize(img_1[i]);
        img_5[i] = binarize(img_5[i]);
        img_10[i] = binarize(img_10[i]);
        img361[i] = binarize(img361[i]);
        img361_1[i] = binarize(img361_1[i]);
        img361_5[i] = binarize(img361_5[i]);
        img361_10[i] = binarize(img361_10[i]);

        double imgBefore = countNonBlackPercentage(img[i]);
        double imgBefore_1 = countNonBlackPercentage(img_1[i]);
        double imgBefore_5 = countNonBlackPercentage(img_5[i]);
        double imgBefore_10 = countNonBlackPercentage(img_10[i]);

        cout << "\n\nRGB - MONO:\n";
        img[i] = subtractImages(img[i], img361[i]);
        cout << countNonBlackPercentage(img[i]) / imgBefore * 100 << "%\n";
        img_1[i] = subtractImages(img_1[i], img361_1[i]);
        cout << countNonBlackPercentage(img_1[i]) / imgBefore_1 * 100 << "%\n";
        img_5[i] = subtractImages(img_5[i], img361_5[i]);
        cout << countNonBlackPercentage(img_5[i]) / imgBefore_5 * 100 << "%\n";
        img_10[i] = subtractImages(img_10[i], img361_10[i]);
        cout << countNonBlackPercentage(img_10[i]) / imgBefore_10 * 100 << "%\n";
    }

    saveImages(img, "_12_rgb_minus_mono", "difference");
    saveImages(img_1, "_12_rgb_minus_mono_1", "difference");
    saveImages(img_5, "_12_rgb_minus_mono_5", "difference");
    saveImages(img_10, "_12_rgb_minus_mono_10", "difference");

     //RGB-361 - MONO
    img = loadImages("_9_sum_hysteresis", 1, "rgb-361");
    img361 = loadImages("_9_hysteresis", 1, "mono");
    img_1 = loadImages("_10_RDP_1", 1, "rgb-361");
    img361_1 = loadImages("_10_RDP_1", 1, "mono");
    img_5 = loadImages("_10_RDP_5", 1, "rgb-361");
    img361_5 = loadImages("_10_RDP_5", 1, "mono");
    img_10 = loadImages("_10_RDP_10", 1, "rgb-361");
    img361_10 = loadImages("_10_RDP_10", 1, "mono");

    for (int i = 0; i < img.size(); ++i) {
        img[i] = binarize(img[i]);
        img_1[i] = binarize(img_1[i]);
        img_5[i] = binarize(img_5[i]);
        img_10[i] = binarize(img_10[i]);
        img361[i] = binarize(img361[i]);
        img361_1[i] = binarize(img361_1[i]);
        img361_5[i] = binarize(img361_5[i]);
        img361_10[i] = binarize(img361_10[i]);

        double imgBefore = countNonBlackPercentage(img[i]);
        double imgBefore_1 = countNonBlackPercentage(img_1[i]);
        double imgBefore_5 = countNonBlackPercentage(img_5[i]);
        double imgBefore_10 = countNonBlackPercentage(img_10[i]);

        cout << "\n\nRGB-361 - MONO:\n";
        img[i] = subtractImages(img[i], img361[i]);
        cout << countNonBlackPercentage(img[i]) / imgBefore * 100 << "%\n";
        img_1[i] = subtractImages(img_1[i], img361_1[i]);
        cout << countNonBlackPercentage(img_1[i]) / imgBefore_1 * 100 << "%\n";
        img_5[i] = subtractImages(img_5[i], img361_5[i]);
        cout << countNonBlackPercentage(img_5[i]) / imgBefore_5 * 100 << "%\n";
        img_10[i] = subtractImages(img_10[i], img361_10[i]);
        cout << countNonBlackPercentage(img_10[i]) / imgBefore_10 * 100 << "%\n";
    }

    saveImages(img, "_12_rgb361_minus_mono", "difference");
    saveImages(img_1, "_12_rgb361_minus_mono_1", "difference");
    saveImages(img_5, "_12_rgb361_minus_mono_5", "difference");
    saveImages(img_10, "_12_rgb361_minus_mono_10", "difference");
}

int main() {
    /*
    extractChannels();
    gaussFilter();
    kirschFilter();
    normalizeKirsch();
    maxOfKirsch();
    gradientDirections();
    nonMaximum();
    
    histerize(vector<vector<vector<int>>>({
        vector<vector<int>>({
            vector<int>({15, 75}),
            vector<int>({15, 75}),
            vector<int>({15, 75}),
            vector<int>({15, 75}),
            vector<int>({15, 75}),
            vector<int>({15, 75}),
            vector<int>({15, 75})
        }),
        vector<vector<int>>({
            vector<int>({6, 60}),
            vector<int>({6, 60}),
            vector<int>({6, 60}),
            vector<int>({6, 60}),
            vector<int>({6, 60}),
            vector<int>({6, 60}),
            vector<int>({6, 60})
        }),
        vector<vector<int>>({
            vector<int>({20, 75}),
            vector<int>({20, 75}),
            vector<int>({20, 75}),
            vector<int>({20, 75}),
            vector<int>({20, 75}),
            vector<int>({20, 75}),
            vector<int>({20, 75})
        })
    }));

    histerizeSum();
    separateCurves();
    rdp(1);
    rdp(5);
    rdp(10);
    */
    colorize();
    difference();
    
    return 0;
}