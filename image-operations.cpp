#include "constants.h"
#include "image-oprations.h"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cmath>


//! Funkcja przeprowadzająca filtracje obrazu przy uzyciu dowolnej maski 
/*
    \param image macierz obrazu wejsciowego
    \param mask macierz maski
    \param maskFactor - 
*/
Mat filter(Mat& image, vector<vector<int>> &mask, bool absolute) {
    Mat clone = image.clone();
    Mat_<int> imageMatrix = image;
    Mat_<int> cloneMatrix = clone;
    double maskFactor = 0.0;
    int shift = floor(mask.size() / 2.0);

    for (int i = 0; i < mask.size(); ++i)
        for (int j = 0; j < mask.size(); ++j)
            maskFactor += abs(mask[i][j]);

        for (int i = 0; i < image.rows; ++i) {
            for (int j = 0; j < image.cols; ++j) {
                double sum = 0;

                for (int a = -shift; a <= shift; ++a) {
                    for (int b = -shift; b <= shift; ++b) {
                        sum += (i + a >= 0 && i + a < image.rows && j + b >= 0 && j + b < image.cols) ?
                            imageMatrix(i+a, j+b) * mask[a + shift][b + shift]
                        :
                            imageMatrix(i, j) * mask[a + shift][b + shift];
                    }
                }
                
                cloneMatrix(i, j) = absolute ? abs(sum / maskFactor) : sum / maskFactor;
            }
        }

    return cloneMatrix;
}

Mat addImages(Mat& imageA, Mat& imageB) {
    Mat clone = imageA.clone();
    Mat_<int> cloneMatrix = clone;
    Mat_<int> imageBMatrix = imageB;

    for (int i = 0; i < imageA.rows; ++i) {
        for (int j = 0; j < imageA.cols; ++j) {
            cloneMatrix(i, j) = int(cloneMatrix(i, j) + imageBMatrix(i, j));
        }
    }

    return cloneMatrix;
}

Mat subtractImages(Mat& imageA, Mat& imageB) {
    Mat clone = imageA.clone();
    Mat_<int> cloneMatrix = clone;
    Mat_<int> imageBMatrix = imageB;

    for (int i = 0; i < imageA.rows; ++i) {
        for (int j = 0; j < imageA.cols; ++j) {
            cloneMatrix(i, j) -= imageBMatrix(i, j);
        }
    }

    return cloneMatrix;
}

Mat maxOfImages(Mat& imageA, Mat& imageB) {
    Mat clone = imageA.clone();
    Mat_<int> cloneMatrix = clone;
    Mat_<int> imageAMatrix = imageA;
    Mat_<int> imageBMatrix = imageB;

    for (int i = 0; i < imageA.rows; ++i) {
        for (int j = 0; j < imageA.cols; ++j) {
              cloneMatrix(i, j) = max(imageAMatrix(i, j), imageBMatrix(i, j));
         }
    }

    return cloneMatrix;
}

Mat normalizeImage(Mat& image) {
    Mat clone(image.rows, image.cols, CV_8U);
    image.convertTo(clone, CV_8U);
    Mat_<int> cloneMatrix = clone;

    int min = 255;
    int max = 0;

    for (int i = 0; i < image.rows; ++i) {
        for (int j = 0; j < image.cols; ++j) {
            if (min > cloneMatrix(i, j))
                min = cloneMatrix(i, j);
            if (max < cloneMatrix(i, j))
                max = cloneMatrix(i, j);
        }
    }

    double ratio = 255.0 / (max - min);
    
    for (int i = 0; i < image.rows; ++i) {
        for (int j = 0; j < image.cols; ++j) {
            cloneMatrix(i, j) = (cloneMatrix(i, j)- min) * ratio;
        }
    }

    return cloneMatrix;
}

Mat nonMaximumSuppresion(Mat& image, Mat& gradientIndices) {
    Mat clone = image.clone();
    Mat_<int> cloneMatrix = clone;
    Mat_<int> imageMatrix = image;
    Mat_<int> gradientIndicesMatrix = gradientIndices;

        for (int i = 0; i < image.rows; ++i) {
            for (int j = 0; j < image.cols; ++j) {
                int neighbourA = imageMatrix(i, j), neighbourB = imageMatrix(i, j);

                switch (gradientIndicesMatrix(i, j)) {
                    case DIRECTION_0:
                        neighbourA = j > 0 ? imageMatrix(i, j-1) : imageMatrix(i, j);
                        neighbourB = j < image.cols - 1 ? imageMatrix(i, j+1) : imageMatrix(i, j);
                        break;
                    case DIRECTION_45:
                        neighbourA = i > 0 && j < image.cols - 1 ? imageMatrix(i-1, j+1) : imageMatrix(i, j);
                        neighbourB = i < image.rows - 1 && j > 0 ? imageMatrix(i+1, j-1) : imageMatrix(i, j);
                        break;
                    case DIRECTION_90:
                        neighbourA = i > 0 ? imageMatrix(i-1, j) : imageMatrix(i, j);
                        neighbourB = i < image.rows - 1 ? imageMatrix(i+1, j) : imageMatrix(i, j);
                        break;
                    case DIRECTION_135:
                        neighbourA = i < image.rows - 1 && j < image.cols - 1 ? imageMatrix(i+1, j+1) : imageMatrix(i, j);
                        neighbourB = i > 0 && j > 0 ? imageMatrix(i-1, j-1) : imageMatrix(i, j);
                        break;
                }

                if (!(imageMatrix(i, j) >= neighbourA && imageMatrix(i, j) > neighbourB))
                    cloneMatrix(i, j) = 0;
            }
        }


    return cloneMatrix;
}

Mat arctanOfImages(Mat& imageA, Mat& imageB) {
    Mat clone(imageA.rows, imageA.cols, imageA.type());
    Mat_<int> imageAMatrix = imageA;
    Mat_<int> imageBMatrix = imageB;
    Mat_<int> cloneMatrix = clone;

    for (int i = 0; i < imageA.rows; ++i) {
        for (int j = 0; j < imageA.cols; ++j) {
            double foo = atan(imageBMatrix(i, j) / (imageAMatrix(i, j) != 0 ? imageAMatrix(i, j) : 0.000001)) + (M_PI / 2.0);
                
            if (foo < M_PI / 4.0)
                cloneMatrix(i, j) = DIRECTION_90;
            else if (foo < M_PI / 2.0)
                 cloneMatrix(i, j) = DIRECTION_135;
            else if (foo < M_PI * 3 / 4.0)
                cloneMatrix(i, j) = DIRECTION_0;
            else 
                cloneMatrix(i, j) = DIRECTION_45;
        }
    }

    return cloneMatrix;
}

Mat hysteresis(Mat& image, int t1, int t2) {
    Mat clone = image.clone();
    Mat_<int> cloneMatrix = image;
    Mat_<int> imageMatrix = image;
    vector<vector<int> > whites;

    for (int i = 0; i < image.rows; ++i) {
        for (int j = 0; j < image.cols; ++j) {
            if (imageMatrix(i, j) > t2) { 
                cloneMatrix(i, j) = 255;
                whites.push_back({i, j});
            }
            else if (imageMatrix(i, j) < t1)
                cloneMatrix(i, j) = 0;
        }
    }

    for (vector<int> v : whites)
        hysteresisFollowing(cloneMatrix, v[0], v[1], t1, t2);

    for (int i = 0; i < image.rows; ++i) 
        for (int j = 0; j < image.cols; ++j) 
            if (cloneMatrix(i, j) != 255)
                cloneMatrix(i, j) = 0;

    return cloneMatrix;
}

void hysteresisFollowing(Mat& image, int i, int j, int t1, int t2) {
    Mat_<int> imageMatrix = image;
    vector<vector<int> > neighbours;

    imageMatrix(i, j) = 255;

    if (j > 0) neighbours.push_back({i, j - 1});
    if (i > 0 && j > 0) neighbours.push_back({i - 1, j - 1});
    if (i > 0) neighbours.push_back({i - 1, j});
    if (i > 0 && j < image.cols - 1) neighbours.push_back({i - 1, j + 1});
    if (j < image.cols - 1) neighbours.push_back({i, j + 1});
    if (i < image.rows - 1 && j < image.cols - 1) neighbours.push_back({i + 1, j + 1});
    if (i < image.rows - 1) neighbours.push_back({i + 1, j});
    if (i < image.rows - 1 && j > 0) neighbours.push_back({i + 1, j - 1});

    for (int a = 0; a < neighbours.size(); ++a) {
        if (imageMatrix(neighbours[a][0], neighbours[a][1]) < t1) {
            imageMatrix(neighbours[a][0], neighbours[a][1]) = 0;
        }
        else {
            if (imageMatrix(neighbours[a][0], neighbours[a][1]) > 0 && imageMatrix(neighbours[a][0], neighbours[a][1]) < 255)
                hysteresisFollowing(image, neighbours[a][0], neighbours[a][1], t1, t2);
        }
    }
}

Mat binarize(Mat& image) {
    Mat foo;
    cvtColor(image, foo, cv::COLOR_BGR2GRAY);
    Mat clone = foo.clone();
    Mat_<int> cloneMatrix = clone;

    for (int i = 0; i < image.rows; ++i) {
        for (int j = 0; j < image.cols; ++j) {
            if (cloneMatrix(i, j) != 0)
                cloneMatrix(i, j) = 255;
        }
    }

    return cloneMatrix;
}

Mat colorize(Mat& binary, Mat& image) {
    Mat clone(Mat::zeros(image.size(), image.type()));
    Mat_<Vec3b> cloneMatrix = clone;
    Mat_<int> binaryMatrix = binary;
    Mat_<Vec3b> imageMatrix = image;

    for (int k = 0; k < image.channels(); ++k) {
        for (int i = 0; i < image.rows; ++i) {
            for (int j = 0; j < image.cols; ++j) {
                if (binaryMatrix(i, j) > 0)
                    cloneMatrix(i, j).val[k] = imageMatrix(i, j).val[k];
            }
        }
    }

    return cloneMatrix;
}

vector<vector<Point>> findCurves(Mat& img) {
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    findContours(img, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

    return contours;
}

vector<Mat> loadImages(string postfix, int mode, string directory) {
    if (directory.length() > 0)
        directory += "/";

    vector<Mat> images;

    for (ImagePath path : imagePaths) {
        stringstream ss;
        ss << IMAGES_DIR << "/" << directory << path.name << postfix << "." << path.ext;
        images.push_back(imread(String(ss.str()), mode)); 
    }

    return images;
}

double countNonBlackPercentage(Mat& image) {
    Mat_<int> cloneMatrix = image;
    int nones = 0;

    for (int i = 0; i < image.rows; ++i)
        for (int j = 0; j < image.cols; ++j) {
            if (cloneMatrix(i, j) > 0)
                ++nones;
        }
        
    return double(nones);
}

void saveImages(vector<Mat>& imgs, string postfix, string directory) {
    if (directory.length() > 0)
        directory += "/";

    for (int i = 0; i < imgs.size(); ++i) {
        stringstream ss;
        ss << IMAGES_DIR << "/" << directory << imagePaths[i].name << postfix << "." << imagePaths[i].ext;
        Mat mat(imgs[i].rows, imgs[i].cols, CV_8UC3);
        imgs[i].convertTo(mat, CV_8UC3);
        imwrite(ss.str(), mat);
    }   
}

void saveImages(vector<vector<Mat>>& imgs, vector<string> postfixes, string directory) {
    if (directory.length() > 0)
        directory += "/";

    for (int i = 0; i < imgs.size(); ++i) {
        for (int j = 0; j < imgs[i].size(); ++j) {
            stringstream ss;
            ss << IMAGES_DIR << "/" << directory << imagePaths[i].name << postfixes[j] << "." << imagePaths[i].ext;
            Mat mat(imgs[i][j].rows, imgs[i][j].cols, CV_8UC3);
            imgs[i][j].convertTo(mat, CV_8UC3);
            imwrite(ss.str(), mat);
        }
    }   
}