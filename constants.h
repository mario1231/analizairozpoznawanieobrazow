#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <vector>
#include <string>

using namespace std;

//! Folder z obrazami wejsciowymi
#define IMAGES_DIR "../render"

struct ImagePath {
    string name;
    string ext;
    ImagePath(string, string);
};

//! Nazwy plikow obrazow wejsciowych
extern vector<ImagePath> imagePaths;

#define DIRECTION_0 180
#define DIRECTION_45 255
#define DIRECTION_90 0
#define DIRECTION_135 60

extern vector<vector<int> > gaussianMask;
extern vector<vector<vector<int> > > kirschMasks;

#endif